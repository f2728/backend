package utils.parser;

import models.Fountain;

import java.util.List;

public class FountainUtility {
    private static List<Fountain> fountains;

    public static List<Fountain> getFountains () {
        if (fountains == null) {
            fountains = BaseParser.parseFile("Data/fountaindata.csv", Fountain.class);
        }

        return fountains;
    }
}
