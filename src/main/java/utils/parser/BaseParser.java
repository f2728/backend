package utils.parser;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class BaseParser {
    public static <T> List<T> parseFile (String fileName, Class<T> typeParameterClass) {
        List<T> result = null;

        try (InputStream is = TreeUtility.class.getClassLoader().getResourceAsStream(fileName)) {

            if (is == null) {
                return null;
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {

                result = new CsvToBeanBuilder(br)
                        .withType(typeParameterClass)
                        .withSkipLines(1)
                        .build()
                        .parse();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
