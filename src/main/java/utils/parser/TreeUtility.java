package utils.parser;

import models.Tree;

import java.util.List;

public class TreeUtility {
    private static List<Tree> trees;

    public static List<Tree> getTrees () {
        if (trees == null) {
            trees = BaseParser.parseFile("Data/treedata.csv", Tree.class);
        }

        return trees;
    }
}

