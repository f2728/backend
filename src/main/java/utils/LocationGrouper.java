package utils;

import models.Coordinate;
import models.Fountain;
import models.Location;
import models.Tree;
import utils.parser.TreeUtility;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocationGrouper {

    private static Dictionary<Coordinate, Location> locationCash = new Hashtable<>();

    public static Location map(Fountain fountain){

        Location cashedLocation = locationCash.get(fountain.getCoordinate());

        if (cashedLocation != null) {
            return cashedLocation;
        }

        List<Tree> trees = TreeUtility.getTrees();

        double minimalX = fountain.getCoordinate().getX() - maximalDistance;
        double maximalX = fountain.getCoordinate().getX() + maximalDistance;

        double minimalY = fountain.getCoordinate().getY() - maximalDistance;
        double maximalY = fountain.getCoordinate().getY() + maximalDistance;

        List<Integer> treesCloseToTheFountain = trees
                .stream()
                .filter(tree -> tree.getCrownDiameter() >= 2)
                .filter(tree -> isCloseEnough(tree.getCoordinate(), minimalX, maximalX, minimalY, maximalY))
                .map(tree -> tree.getTreeId())
                .collect(Collectors.toList());

        Location location = new Location(fountain.getCoordinate(), treesCloseToTheFountain);

        locationCash.put(location.getCoordinate(), location);

        return location;
    }

    private static final double maximalDistance = 0.0005;

    private static boolean isCloseEnough(Coordinate source, double minimalX, double maximalX, double minimalY, double maximalY){
        double x = source.getX();
        double y = source.getY();

        return x > minimalX && x < maximalX && y > minimalY && y < maximalY;
    }

    public static double distanceTo(double x, double y, Coordinate coordinate) {
        double distance = Math.pow(Math.abs(x - coordinate.getX()), 2) + Math.pow(Math.abs(y - coordinate.getY()), 2);

        return distance;
    }
}
