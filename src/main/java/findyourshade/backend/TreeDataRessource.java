package findyourshade.backend;

import com.google.gson.Gson;
import models.Tree;
import utils.parser.TreeUtility;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.stream.Collectors;


@Path("/tree-data")
public class TreeDataRessource {
    @GET
    @Produces("application/json")
    public String get() {
        List<Tree> trees = TreeUtility.getTrees();

        List<Tree> treeList = trees.stream().limit(10).collect(Collectors.toList());

        String jsonString = new Gson().toJson(treeList);

        return jsonString;
    }
}

