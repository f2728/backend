package findyourshade.backend;

import com.google.gson.Gson;
import models.Location;
import models.Tree;
import utils.LocationGrouper;
import utils.parser.FountainUtility;
import utils.parser.TreeUtility;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Path("/locations")
public class LocationRessource {
    @GET
    @Produces("application/json")
    public String get(@QueryParam("x") double x, @QueryParam("y") double y) {

        List<Location> locations = FountainUtility
                .getFountains()
                .stream()
                .sorted(Comparator.comparingDouble(fountain -> LocationGrouper.distanceTo(x, y, fountain.getCoordinate())))
                .limit(100)
                .map(f -> LocationGrouper.map(f))
                .filter(l -> l.getTreeCount() > 5)
                .collect(Collectors.toList());

        String jsonString = new Gson().toJson(locations);

        return jsonString;
    }
}

