package findyourshade.backend;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;


@ApplicationPath("/api")
public class ApiApplication extends Application {
    //private Set<Object> singletons = new HashSet<Object>();

    //public ApiApplication(){
    //    CorsFilter corsFilter = new CorsFilter();
    //    corsFilter.getAllowedOrigins().add("*");
    //    corsFilter.setAllowedMethods("OPTIONS, GET, POST, DELETE, PUT, PATCH");
    //    singletons.add(corsFilter);
    //}

    //@Override
    //public Set<Object> getSingletons() {
    //    return singletons;
    //}
}