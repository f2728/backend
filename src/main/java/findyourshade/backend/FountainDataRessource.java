package findyourshade.backend;

import com.google.gson.Gson;
import models.Coordinate;
import models.Fountain;
import utils.parser.FountainUtility;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.stream.Collectors;

@Path("/fountain-data")
public class FountainDataRessource {
    @GET
    @Produces("application/json")
    public String get() {
        List<Fountain> fountains = FountainUtility.getFountains();

        List<Coordinate> fountainList = fountains.stream().limit(10).map(fountain -> fountain.getCoordinate()). collect(Collectors.toList());

        String jsonString = new Gson().toJson(fountainList);

        return jsonString;
    }
}
