package models;

import com.opencsv.bean.CsvBindByPosition;

public class Tree {

    @CsvBindByPosition(position = 3)
    private int treeId;

    @CsvBindByPosition(position = 2)
    private String point;

    @CsvBindByPosition(position = 15)
    private String crownDiameter;

    @CsvBindByPosition(position = 8)
    private String treeType;

    public int getTreeId(){
        return treeId;
    }

    public int getCrownDiameter(){
        return Integer.parseInt(crownDiameter);
    }

    public Coordinate getCoordinate(){
        return new Coordinate(point);
    }

}
