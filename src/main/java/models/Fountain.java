package models;

import com.opencsv.bean.CsvBindByPosition;

public class Fountain {
    @CsvBindByPosition(position = 1)
    private int fountainId;

    @CsvBindByPosition(position = 2)
    private String point;

    public Coordinate getCoordinate(){
        return new Coordinate(point);
    }
}
