package models;

import java.util.List;

public class Location {
    private Coordinate fountainCoordinate;
    private Integer treeCount;

    public Location(Coordinate coordinate, List<Integer> treeIds) {
        this.fountainCoordinate = coordinate;
        this.treeCount = treeIds.size();
    }

    public Coordinate getCoordinate() {
        return fountainCoordinate;
    }

    public Integer getTreeCount() {
        return treeCount;
    }
}
